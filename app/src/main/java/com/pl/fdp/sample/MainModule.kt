package com.pl.fdp.sample

import android.content.Context
import android.content.SharedPreferences
import androidx.preference.PreferenceManager
import com.pl.library.fdp.core.FanDataPlatform
import com.pl.library.fdp.core.consent.FdpConsentManager
import com.pl.library.fdp.core.events.FdpEventTracker
import com.pl.library.fdp.core.widgets.FdpWidgetProvider
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object MainModule {

    @Provides
    fun providePrefsManager(@ApplicationContext context: Context): SharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

    @Singleton
    @Provides
    fun provideFdpConfig(@ApplicationContext context: Context): FanDataPlatform = FanDataPlatform.newSession()
        //TODO replace with your instance urls for events and widgets 
        .setEventsURL("https://events.example.host.com")
        .setWidgetsURL("https://widget-configuration.example.host.com")
        .setEventsSyncDelay(5, TimeUnit.SECONDS)
        .enableLogging(true) // Not recommended for production code
        .setConsentManager(FdpConsentManager.OptIn)
        .start(context)

    @Singleton
    @Provides
    fun provideFdpEventTracker(fanDataPlatform: FanDataPlatform): FdpEventTracker = fanDataPlatform.eventTracker

    @Singleton
    @Provides
    fun provideFdpWidgetProvider(fanDataPlatform: FanDataPlatform): FdpWidgetProvider = fanDataPlatform.widgetProvider
}