package com.pl.fdp.sample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.button.MaterialButton
import com.pl.fdp.sample.groupie.GroupieSampleFragment
import com.pl.fdp.sample.recyclerview.RecyclerViewSampleFragment
import dagger.hilt.android.AndroidEntryPoint

@ExperimentalStdlibApi
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        findViewById<MaterialButton>(R.id.recycler_sample_btn).apply {
            setOnClickListener {
                if (savedInstanceState == null) {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, RecyclerViewSampleFragment.newInstance())
                        .commitNow()
                }
            }
        }

        findViewById<MaterialButton>(R.id.groupie_sample_btn).apply {
            setOnClickListener {
                if (savedInstanceState == null) {
                    supportFragmentManager.beginTransaction()
                        .replace(R.id.container, GroupieSampleFragment.newInstance())
                        .commitNow()
                }
            }
        }
    }
}