package com.pl.fdp.sample.groupie

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.pl.fdp.sample.R
import com.pl.fdp.sample.RecyclerDecoration
import com.pl.library.fdp.core.widgets.interstitial.FdpWidgetInterstitial
import com.pl.library.fdp.core.widgets.isInterstitial
import com.pl.library.fdp.core.widgets.isPopup
import com.pl.library.fdp.core.widgets.popup.FdpWidgetPopup
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import dagger.hilt.android.AndroidEntryPoint

@ExperimentalStdlibApi
@AndroidEntryPoint
class GroupieSampleFragment : Fragment() {

    companion object {

        private const val GRID_SPAN_COUNT = 2
        fun newInstance() = GroupieSampleFragment()
    }

    private val viewModel: GroupieSampleViewModel by viewModels()

    private val groupieAdapter = GroupAdapter<GroupieViewHolder>().apply { spanCount = GRID_SPAN_COUNT }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        return inflater.inflate(R.layout.groupie_sample_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(view.findViewById<RecyclerView>(R.id.groupie_main_recycler_view)) {
            (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
            initGroupieRecycler()
        }

        viewModel.groupieItems.observe(viewLifecycleOwner) { data ->
            groupieAdapter.update(data)
        }


        viewModel.fdpWidgetResult.observe(viewLifecycleOwner) { data ->
            when {
                data.isInterstitial() -> {
                    FdpWidgetInterstitial.start(requireContext(), data)
                }
                data.isPopup() -> {
                    FdpWidgetPopup.start(childFragmentManager, data)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.trackScreenView()
    }

    private fun RecyclerView.initGroupieRecycler() {
        adapter = groupieAdapter

        //Setup for splitting the UI items in a grid
        layoutManager = GridLayoutManager(context, groupieAdapter.spanCount).apply {
            spanSizeLookup = groupieAdapter.spanSizeLookup
        }
        //You can as many decorations as needed
        addItemDecoration(RecyclerDecoration())

        setPadding(left, top, right, 16)
    }
}