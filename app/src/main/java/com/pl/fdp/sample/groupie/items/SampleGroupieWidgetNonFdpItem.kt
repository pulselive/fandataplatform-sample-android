package com.pl.fdp.sample.groupie.items

import android.widget.TextView
import com.pl.fdp.sample.R
import com.xwray.groupie.GroupieViewHolder
import com.xwray.groupie.Item

class SampleGroupieWidgetNonFdpItem : Item<GroupieViewHolder>() {

    override fun bind(viewHolder: GroupieViewHolder, position: Int) {
        with(viewHolder.itemView) {
            findViewById<TextView>(R.id.item_text).let {
                it.text = "${it.text} $position"
            }
        }
    }

    override fun getLayout(): Int = R.layout.sample_domain_custom_item

    //When you override the span size in this particular case your UI items will appear in a grid of two items per row
    override fun getSpanSize(spanCount: Int, position: Int) = spanCount / 2
}
