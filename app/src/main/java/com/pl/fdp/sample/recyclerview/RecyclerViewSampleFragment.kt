package com.pl.fdp.sample.recyclerview

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.SimpleItemAnimator
import com.pl.fdp.sample.MainViewModel
import com.pl.fdp.sample.R
import com.pl.fdp.sample.RecyclerDecoration
import com.pl.library.fdp.core.widgets.interstitial.FdpWidgetInterstitial
import com.pl.library.fdp.core.widgets.isInterstitial
import com.pl.library.fdp.core.widgets.isPopup
import com.pl.library.fdp.core.widgets.popup.FdpWidgetPopup
import dagger.hilt.android.AndroidEntryPoint

@ExperimentalStdlibApi
@AndroidEntryPoint
class RecyclerViewSampleFragment : Fragment() {

    companion object {

        fun newInstance() = RecyclerViewSampleFragment()
    }

    private val viewModel: MainViewModel by viewModels()
    private val recyclerViewSampleAdapter: RecyclerViewSampleAdapter = RecyclerViewSampleAdapter()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        return inflater.inflate(R.layout.recyclerview_sample_fragment, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        with(view.findViewById<RecyclerView>(R.id.main_recycler_view)) {
            (itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
            adapter = recyclerViewSampleAdapter
            addItemDecoration(RecyclerDecoration())
        }

        viewModel.items.observe(viewLifecycleOwner) { data ->
            recyclerViewSampleAdapter.updateAsync(data)
        }

        viewModel.fdpWidgetResult.observe(viewLifecycleOwner) { data ->
            when {
                data.isInterstitial() -> {
                    FdpWidgetInterstitial.start(requireContext(), data)
                }
                data.isPopup() -> {
                    FdpWidgetPopup.start(childFragmentManager, data)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        viewModel.trackScreenView()
    }
}