package com.pl.fdp.sample

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pl.library.fdp.core.events.FdpEventAttribute
import com.pl.library.fdp.core.events.FdpEventTracker
import com.pl.library.fdp.core.widgets.FdpWidgetProvider
import com.pl.library.fdp.core.widgets.FdpWidgetResult
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalStdlibApi
@HiltViewModel
class MainViewModel @Inject constructor(
    private val fdpWidgetProvider: FdpWidgetProvider,
    private val fdpEventTracker: FdpEventTracker,
) : ViewModel() {

    val items: LiveData<List<ClientSampleItem>>
        get() = _items

    private val _items: MutableLiveData<List<ClientSampleItem>> = MutableLiveData()

    val fdpWidgetResult: LiveData<FdpWidgetResult.Success>
        get() = _fdpWidgetResult

    private val _fdpWidgetResult: MutableLiveData<FdpWidgetResult.Success> = MutableLiveData()

    init {
        viewModelScope.launch(Dispatchers.IO + CoroutineExceptionHandler { _, throwable ->
            Log.e("FDP Sample App", "Unexpected Error", throwable)
        }) {
            // Load domain data items
            val items = mutableListOf<ClientSampleItem>()
            IntRange(0, 19).forEach { _ -> items.add(ClientSampleItem.DomainItem) }
            _items.postValue(items)

            //this could be any of the supported widget types
            val widget = fdpWidgetProvider(EXAMPLE_PLACEMENT_ID)

            if (widget !is FdpWidgetResult.Success) return@launch

            // Combine widget data with domain
            val adjustedResults = items.toMutableList()
            adjustedResults.add(3, ClientSampleItem.WidgetItem(widget)) // insert FDP slot at desired location
            _items.postValue(adjustedResults)
            _fdpWidgetResult.postValue(widget)
        }
    }

    fun trackScreenView() {
        val payload = mapOf("Custom schema key" to FdpEventAttribute.StringAttr("Custom schema value"))
        fdpEventTracker.trackScreenView("Sample Screen", payload)
    }

    companion object {

        private const val EXAMPLE_PLACEMENT_ID = "ae821fd7-2162-4a87-babe-88939977a15b"
    }
}