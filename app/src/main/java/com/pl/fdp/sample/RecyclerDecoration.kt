package com.pl.fdp.sample

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.pl.fdp.sample.R

class RecyclerDecoration : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView, state: RecyclerView.State) {
        super.getItemOffsets(outRect, view, parent, state)
        val spacing = view.resources.getDimension(R.dimen.spacing).toInt()
        outRect.set(spacing, 0, spacing, spacing)

        val adapterPos = parent.getChildAdapterPosition(view)
        if (adapterPos == 0) {
            outRect.top = spacing
        }
        if (adapterPos == parent.adapter?.itemCount ?: 0 - 1) {
            outRect.bottom = spacing
        }
    }
}