package com.pl.fdp.sample

import com.pl.library.fdp.core.widgets.FdpWidgetResult

sealed class ClientSampleItem {

    object DomainItem: ClientSampleItem()

    data class WidgetItem(val widgetResult: FdpWidgetResult): ClientSampleItem()
}