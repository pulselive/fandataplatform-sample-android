package com.pl.fdp.sample.groupie

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pl.fdp.sample.groupie.items.SampleGroupieWidgetNonFdpItem
import com.pl.library.fdp.core.events.FdpEventAttribute
import com.pl.library.fdp.core.events.FdpEventTracker
import com.pl.library.fdp.core.widgets.FdpWidgetProvider
import com.pl.library.fdp.core.widgets.FdpWidgetResult
import com.pl.library.fdp.widgets.groupie.FdpWidgetGroup
import com.xwray.groupie.Group
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalStdlibApi
@HiltViewModel
class GroupieSampleViewModel @Inject constructor(
    private val fdpWidgetProvider: FdpWidgetProvider,
    private val fdpEventTracker: FdpEventTracker,
) : ViewModel() {

    val groupieItems: LiveData<List<Group>>
        get() = _groupieItems

    private val _groupieItems: MutableLiveData<List<Group>> = MutableLiveData()

    val fdpWidgetResult: LiveData<FdpWidgetResult.Success>
        get() = _fdpWidgetResult

    private val _fdpWidgetResult: MutableLiveData<FdpWidgetResult.Success> = MutableLiveData()

    init {
        viewModelScope.launch(Dispatchers.IO + CoroutineExceptionHandler { _, throwable ->
            Log.e("FDP Sample App", "Unexpected Error", throwable)
        }) {
            // Load non-fdp items (some items that are specific to the client app)
            val nonFdpItems = mutableListOf<Group>()
            IntRange(0, 19).forEach { _ -> nonFdpItems.add(SampleGroupieWidgetNonFdpItem()) }
            _groupieItems.postValue(nonFdpItems)

            //this could be any of the supported widget types
            val fdpWidgetData: FdpWidgetResult = fdpWidgetProvider(EXAMPLE_PLACEMENT_ID)

            if (fdpWidgetData !is FdpWidgetResult.Success) return@launch

            // Combine non-fdp items with fdp groupie items; notice FdpWidgetGroup(fdpWidgetData)
            val adjustedResults = nonFdpItems.toMutableList()
            adjustedResults.add(3, FdpWidgetGroup(fdpWidgetData)) // insert FDP slot at desired location
            _groupieItems.postValue(adjustedResults)
            _fdpWidgetResult.postValue(fdpWidgetData)
        }
    }

    fun trackScreenView() {
        val payload = mapOf("Custom schema key" to FdpEventAttribute.StringAttr("Custom schema value"))
        fdpEventTracker.trackScreenView("Sample Screen", payload)
    }

    companion object {

        private const val EXAMPLE_PLACEMENT_ID = "ae821fd7-2162-4a87-babe-88939977a15b"
    }
}