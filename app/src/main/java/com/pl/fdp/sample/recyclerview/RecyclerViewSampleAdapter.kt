package com.pl.fdp.sample.recyclerview

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.pl.fdp.sample.ClientSampleItem
import com.pl.fdp.sample.R
import com.pl.library.fdp.widgets.recyclerview.FdpWidgetSubAdapter

class RecyclerViewSampleAdapter(
    private val widgetSubAdapter: FdpWidgetSubAdapter = FdpWidgetSubAdapter(),
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val diff = object : DiffUtil.ItemCallback<ClientSampleItem>() {
        override fun areItemsTheSame(oldItem: ClientSampleItem, newItem: ClientSampleItem): Boolean = oldItem === newItem

        override fun areContentsTheSame(oldItem: ClientSampleItem, newItem: ClientSampleItem): Boolean = oldItem == newItem
    }

    private val differ: AsyncListDiffer<ClientSampleItem> = AsyncListDiffer(this, diff)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        // Try to handle FDP items first
        val widgetViewHolder = widgetSubAdapter.createViewHolder(parent, viewType)
        if (widgetViewHolder != null) return widgetViewHolder

        // If the result is null then this item is not from the FDP domain
        val view = LayoutInflater.from(parent.context).inflate(R.layout.sample_domain_custom_item, parent, false)
        return SampleViewHolder(view)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (val dataItem = differ.currentList[position]) {
            is ClientSampleItem.DomainItem -> {
                // Bind domain data
            }
            is ClientSampleItem.WidgetItem -> {
                // Bind FDP result
                widgetSubAdapter.bindViewHolder(holder, dataItem.widgetResult)
            }
        }
    }

    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        super.onViewRecycled(holder)
        widgetSubAdapter.unBindViewHolder(holder)
    }

    override fun getItemCount(): Int = differ.currentList.size

    override fun getItemViewType(position: Int): Int = when (val dataItem = differ.currentList[position]) {
        //This is a non-FDP item that the client defined
        is ClientSampleItem.DomainItem -> R.layout.sample_domain_custom_item
        //This is a client defined item that wraps an FDP result
        is ClientSampleItem.WidgetItem -> widgetSubAdapter.getItemType(dataItem.widgetResult)
    }

    fun updateAsync(newData: List<ClientSampleItem>) {
        differ.submitList(newData)
    }
}