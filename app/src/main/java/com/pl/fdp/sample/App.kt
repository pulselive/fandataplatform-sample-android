package com.pl.fdp.sample

import android.app.Application
import androidx.preference.PreferenceManager
import com.pl.library.fdp.core.events.FdpEventTracker
import dagger.hilt.android.HiltAndroidApp
import javax.inject.Inject

@HiltAndroidApp
class App : Application() {

    @Inject lateinit var fdpEventTracker: FdpEventTracker

    override fun onCreate() {
        super.onCreate()
        fdpEventTracker.setUserIdentity(DEFAULT_USER_ID, DEFAULT_SSO_PROVIDER)

        /*
           We do this to for demonstration purposes only. This grants user consent for event tracking. In production, applications
           are required by law to gain user consent through transparent and legitimate means.
        */
        fdpEventTracker.updateConsent(true)
    }

    companion object {
         const val DEFAULT_USER_ID = "7475d20b421250d77f87a41974446187"
         const val DEFAULT_SSO_PROVIDER = "My SSO Provider"
    }
}